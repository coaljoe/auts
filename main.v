module main

fn main() {
	println("main()")

	mut e := new_elem()
	e.name = "wood"
	e.elem_type = .wood
	e.density = 100
	e.flameable = true
	_ = e

	println("e: ${e}")

	mut e2 := new_elem()
	e2.name = "sand"
	e2.elem_type = .sand
	e2.density = 2000

	println("e2: ${e2}")

	mut e3 := new_elem()
	e3.name = "glass"
	e3.elem_type = .glass
	e3.density = 2200

	mut e4 := new_elem()
	e4.name = "stone"
	e4.elem_type = .stone
	e4.density = 8000

	mut e_water := new_elem()
	e_water.name = "water"
	e_water.elem_type = .water
	e_water.density = 10000
	e_water.liquid = true

	mut e_concrete := new_elem()
	e_concrete.name = "concrete"
	e_concrete.elem_type = .concrete
	e_concrete.density = 20000

	//
	// Effects
	//
	
	mut ef := new_effect()
	ef.name = "melt"
	ef.in_elem = e2
	ef.out_elem = e3
	ef.cond_temp = 5000

	// XXX bug: can't print recursive elems?
	//println("ef: ${ef}")

	//
	// Objects
	//

	mut ob := new_object()
	ob.name = "stone1"
	ob.dim_x = 2
	ob.dim_y = 2
	ob.dim_z = 2
	ob.elem = e4

	// Position
	ob.transform.pos_x = 10.0
	ob.transform.pos_y = 10.0
	ob.transform.rot_z = 45.0

	// XXX
	//println("ob: ${ob}")

	//
	// Processors
	//

	mut prc := new_processor()
	prc.name = "crushed_stone"
	prc.in_elem = e4
	prc.out_elem = e4
	prc.output_factor = 0.6
	prc.req_electricity = true
	prc.req_power = 1000

	// XXX
	//println("prc: ${prc}")

	//
	// Universal Processors?
	// 

	/*
	prc2 := new_generic_processor()
	prc2.name = "generic_crusher"
	prc2.req_solid = true
	//prc2.in_elem =
	prc2.process(ob)
	*/

	mut gcp := new_generic_crusher_processor()
	gcp.process(ob)

	//e4.generic_processor0 = gcp

	// Try to process itself

	//ob.elem.generic_processor0.process(ob)
	//e4.generic_processor0.process(ob)

	//
	// World
	//

	mut w := new_world()

	b1 := Bit{
		elem_type: .wood,
	}
	
	w.bits[0][0] = b1

	println("w b1: ${w.bits[0][0]}")
	println("w 1 0: ${w.bits[1][0]}")
	//println("w: ${w}")
	//println("w: ${w.bits}")

	//
	// Complex object
	//

	mut ob2 := new_object()
	ob2.name = "pipe" // Reinforced-concrete pipe, a 3d object
	ob2.elem = e_concrete

	println("done")
}
