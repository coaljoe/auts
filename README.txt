auts

auts is a resource-based economy test simulator
use this project whatever you like with or without credit

license: zlib (see LICENSE.txt)


how to run:

1. install latest V (vlang.io)
2. $ v install nsauzede.vsdl2
3. $ v install spytheman.vperlin
4. $ cd isotest/
5. $ ./run.sh


controls:
- space: build wall
- arrows: scroll map