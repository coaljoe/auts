#!/bin/env python

import sys

if len(sys.argv) < 3:
    print("usage: %s mat_density model_mass" % sys.argv[0])
    exit()

mat_density = int(sys.argv[1])

model_mass = int(sys.argv[2])

mat_volume = model_mass / mat_density
result_bits = mat_volume / (1.0/100)

print("mat_density:", mat_density)
print("model_mass:", model_mass)
print("")
print("result:")
print("mat_volume:", mat_volume)
print("result_bits:", result_bits)
print("")
print("done")
