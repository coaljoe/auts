module main

struct Processor {
mut:
	name string
	in_elem &Elem
	out_elem &Elem

	output_factor f32

	cond_temp int
	req_electricity bool
	req_power int
	req_steam int
	req_water int
}

fn new_processor() &Processor {
	p := &Processor{
		in_elem: &Elem(0),
		out_elem: &Elem(0),
	}
	return p
}
