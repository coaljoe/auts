module main

// Object bit
//  size: 10x10x10 (?)
//
// XXX cannot be compound?
struct ObjectBit {
	elem_type ElemType
}

struct Object {
mut:
	name string
	dim_x int // Approx.
	dim_y int
	dim_z int
	elem &Elem // XXX use id/enum

	// Position in the world
	//pos_x int
	//pos_y int
	//pos_z int

	// Flixible position in the world
	transform &Transform

	// Bits of object's voxel model
	//bits [10][10]ObjectBit
}

fn new_object() &Object {
	o := &Object{
		elem: &Elem(0),
		transform: new_transform(),
	}
	return o
}
