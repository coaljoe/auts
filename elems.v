module main

enum ElemType {
	none_ // None/Empty
	
	//// Primary form
	wood sand stone coal clay iron
	// rock -> stone (?)
	// Liquid
	water oil lava
	// Gas(?)
	vapour methane
	// Extra(?)
	soil biomass silt
	
	//// Secondary form
	glass concrete brick steel plastic cement
	// Liquid
	// XXX simplify: no need for special-state concrete,
	// use basic type
	//liquid_concrete,

	//// Compounds(?)
	// Ores
	iron_ore
	aluminium_ore // Bauxite
	gold_ore
	silver_ore
	platinum_ore // (?)
	copper_ore
	nickel_ore // Garnierite
	zinc_ore
	lead_ore
	uranium_ore
	//alumina, // Aluminium oxide
	// Construction
	// XXX add only as a subtype of concrete? only useful for recycling information...
	
	reinforced_concrete // Approx 90% concrete, 10% steel (on average)
	

}

struct Elem {
mut:
	name string
	elem_type ElemType
	density int // XXX per bit?
	flameable bool
	liquid bool

	// when
	//when_crushed_fn fn()
	//when_crushed_effects []Effect
	//when_crushed_out_elems []Elem

	// list of processors
	//generic_processors []GenericProcessorI // XXX no interface arrays
	//generic_processors map[string]GenericProcessorI
	//generic_processor0 &GenericProcessorI
	//generic_processor0 &GenericCrusherProcessor
}

fn new_elem() &Elem {
	/*
	e := &Elem{
		//when_crushed_effects: []Effect{},
		//when_crushed_out_elems: []Elem{},
		//generic_processor0: none,
	}
	*/
	e := &Elem{}
	return e
}
