module main

// Point sprite
struct Sprite {
mut:
	// Pos (world-space)
	// center bottom position(?)
	x int
	y int

	w int
	h int

	// Multi-tile
	num_tiles_x int
	num_tiles_y int
	num_tiles int 
	tile_size int

	shift_x int
	// yCorr
	shift_y int
	tex &Texture

	// XXX disable automatic draw: fixme?
	visible bool
	
	spawned bool
}

fn new_sprite() &Sprite {
	s := &Sprite{
		tex: &Texture(0),
		visible: true,
	}

	// Add to SpriteSys
	game.spritesys.add(s)
	return s
}

fn (mut s Sprite) toggle_visible() {
	s.visible = !s.visible
}

fn (mut s Sprite) load(path string) {
	s.tex = new_texture()
	s.tex.load(path)

	s.w, s.h = s.tex.size()

	// Calc shift
	s.shift_x = s.w / 2 // half tile

	//s.shift_y = s.h - (c_cell_h)
	s.shift_y = s.h - (c_cell_h / 2)
	
}

fn (mut s Sprite) spawn() {

}

fn (mut s Sprite) draw() {
	println("sprite draw()")

	if !s.visible {
		return
	}
	
	//s.tex.draw_pos(s.x, s.y)

	px := s.x - s.shift_x
	py := s.y - s.shift_y

	sx, sy := world_to_screen_pos(px, py)
	
	s.tex.draw_pos(sx, sy)
}

fn (mut s Sprite) force_draw() {
	// Force draw
	if !s.visible {
		s.toggle_visible()
		s.draw()
		s.toggle_visible()
	} else {
		s.draw()
	}
}

fn (mut s Sprite) update(dt f32) {

}