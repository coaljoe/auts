module main

struct System {
mut:
	name string
	desc string
	//elems []any
	//elems map[string]any
	//elems map[string]voidptr
	elems []voidptr
}

fn new_system(name, desc string) &System {
	println("new_system(): name $name, desc: $desc")
	
	s := &System{
		name: name,
		desc: desc
	}

	return s
}

fn (mut s System) has_elem(el voidptr) bool {
	for x in s.elems {
		if x == el {
			return true
		}
	}
	return false
}

fn (s &System) idx_elem(el voidptr) int {
	mut found_index := -1
	for i, x in s.elems {
		if x == el {
			found_index = i
			break
		}
	}
	return found_index
}

//fn (mut s System) add_elem(el any) {
fn (mut s System) add_elem(el voidptr) {
	//s.elems << el
	//s.elems["test"] = el
	if s.has_elem(el) {
		//println("already have element $el $&el in system '$s.name'")
		println("already have element $el in system '$s.name'")
		println("addElem failed")
		pp(1) // XXX fixme
	}
	s.elems << el
	println("added element '${typeof(el)}' to system '$s.name'")
}

fn (mut s System) remove_elem(el voidptr) {
	if !s.has_elem(el) {
		println("element not found $el in system '$s.name'")
		println("removeElem failed")
		pp(1) // XXX fixme
	}

	idx := s.idx_elem(el)
	s.elems.delete(idx)		
}

fn (s &System) size() int {
	return s.elems.len
}

fn (mut s System) clear_elems() {
	s.elems.clear()
}

fn (s &System) list_elems() {
	println("panic: not implemented")
	pp(1) // XXX fixme
}