module main

import bits

struct Wall {
mut:
	obj &Obj // Embed
	bits_obj &bits.Object

	// Map obj
	//cx int
	//cy int

	// Size
	sx int
	sy int
	sz int

	view &WallView
}

fn new_wall() &Wall {
	println("new_wall()")

	mut w := &Wall{
		obj: new_obj(),
		bits_obj: &bits.Object(0),

		sx: 1,
		sy: 1,
		sz: 6, // units/tiles/steps?

		view: &WallView(0),
	}

	// Bits Object
	{
		mut bo := bits.new_object()
		bo.name = "wall"
	
		bo.mat_info.add_elem_from_size(w.sx, w.sy, w.sz, bits_ctx.elem_lib["stone"])
		w.bits_obj = bo
	}

	return w
}

//fn (mut w Wall) place_at_check(cx, cy int) ?string {
fn (mut w Wall) place_at_check(cx, cy int) ? {
//fn (mut w Wall) place_at_check(cx, cy int) err {
	mut c := game.field.get_cell(cx, cy)
	if !c.open {
		println("return error")
		return error("can't place wall: cell is not open: cx: $cx, cy: $cy")
	}

	w.obj.set_cx(cx)
	w.obj.set_cy(cy)

	//mut c := &game.field.cells[cx][cy]
	//mut c := game.field.get_cell(cx, cy)
	c.wall = w
	c.open = false

	println("return none")
	return none
}

fn (mut w Wall) place_at(cx, cy int) {
	println("wall place_at: cx: $cx, cy: $cy")

	w.place_at_check(cx, cy) or {
		println("err:")
		println(err)
		println(isnil(err))
		panic(err)
		return
	}
}

fn (mut w Wall) spawn() {
	println("wall spawn()")

	w.view = new_wallview(w)
	w.view.spawn()
}

fn (mut w Wall) update(dt f32) {

}
