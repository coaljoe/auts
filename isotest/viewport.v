module main

import math

struct Viewport {
mut:
	x          f32  // Internal scrolling value in floating point
	y          f32
	shx        int  // Viewport's shift in pixels
	shy        int
	w          int  // Viewport width and height in pixels
	h          int
	mcx        int  // Mouse cell x,y
	mcy        int
}

fn new_viewport() &Viewport {
	v := &Viewport{
		w: ctx.vars.res_x,
		h: ctx.vars.res_y,
	}

	return v
}

fn (mut v Viewport) set_pos(x, y f32) {
	v.x = x
	v.y = y
}

fn (mut v Viewport) update(dt f32) {
	ctx.app.pump_events()
	//mx, my, _ := sdl.GetMouseState()
	//mx, my := vars.mx, vars.my
	mx, my := ctx.app.get_mouse_state()

	has_focus := true

	// Should mouse scroll
	m_scroll := if 
		ctx.vars.mouse_scroll && has_focus
		{ true } else { false }

	mut vx := f32(0.0)
	mut vy := f32(0.0)

	//state := sdl.GetKeyboardState()
	//state := ctx.app.get_keyboard_state()
	scroll_amt := ctx.vars.scroll_speed * dt

	/*
	if vars.dev {
		//pp(2)
		key1 := game.keymap.getKey1("scrollFastKey")
		key2 := game.keymap.getAltKey1("scrollFastKey")
		skey1 := sdl.GetScancodeFromKey(key1)
		skey2 := sdl.GetScancodeFromKey(key2)
		if state[skey1] > 0 || state[skey2] > 0 {
			scrollAmt = vars.scrollSpeedFast * dt
		}
	}
	*/

	//key := game.keymap.getKey1("scrollUpKey")
	mut key := C.SDLK_UP
	mut pressed := ctx.app.is_key_pressed(key)
	if pressed || (m_scroll && my >= 0 && int(my) < ctx.vars.scroll_padding) {
		//pp(2)
		//game.vp.shx += int(vars.scrollSpeed * dt)
		//v.y += scrollAmt
		vy = -scroll_amt
		//p(game.vp.shx)
	}

	//key = game.keymap.getKey1("scrollDownKey")
	key = C.SDLK_DOWN
	pressed = ctx.app.is_key_pressed(key)
	if pressed || (m_scroll && my >= 0 && int(my) > ctx.vars.res_y-ctx.vars.scroll_padding) {
		//v.y -= scrollAmt
		vy = scroll_amt
	}

	//key = game.keymap.getKey1("scrollLeftKey")
	key = C.SDLK_LEFT
	pressed = ctx.app.is_key_pressed(key)
	if pressed || (m_scroll && mx >= 0 && int(mx) < ctx.vars.scroll_padding) {
		//v.x += scrollAmt
		vx = -scroll_amt
	}

	//key = game.keymap.getKey1("scrollRightKey")
	key = C.SDLK_RIGHT
	pressed = ctx.app.is_key_pressed(key)
	if pressed || (m_scroll && mx >= 0 && int(mx) > ctx.vars.res_x-ctx.vars.scroll_padding) {
		//v.x -= scrollAmt
		vx = scroll_amt
	}

	// Hack to make diagonal movement less speedy and more uniform
	// Not optimized
	if vx != 0.0 && vy != 0.0 {
		v_ := f32(math.sqrt(2) / 2.0) // 0.707... (half-diagonal)
		//p(vx, vy)
		vx *= v_
		vy *= v_
		//p(vx, vy)
		//pp(2)
	}

	// Update viewport vars
	v.x += vx
	v.y += vy

	// Update shift
	v.shx = int(math.round(v.x))
	v.shy = int(math.round(v.y))

	//p(">>>", v.shx, v.shy)

	// Update mcx, mcy
	// XXX duplicate of hud.cx/cy?
	if game.field.generated {
		/*
		   v.mcx = wrapCoordX((v.shx + vars.mx) / cell_size)
		   v.mcy = wrapCoordY((v.shy + vars.my) / cell_size)
		*/
		v.mcx = game.hud.mcx
		v.mcy = game.hud.mcy
	}
}
