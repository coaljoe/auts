module main

import os
//import strconv

struct FieldView {
mut:
	m &Field
	tiles map[string]&Texture
	tile_names []string // tile_id -> tile_name
}

fn new_fieldview(m &Field) &FieldView {
	v := &FieldView{
		m: m,
	}

	return v
}

fn (mut v FieldView) load() {
	println("fieldview load()")

	if isnil(ctx) {
		println("ctx is nil")
		pp(2)
	}
	
	//path := "res/images/tiles/test1"
	path := v.m.tiles_path

	mut tile_idx := 0
	for f in os.walk_ext(path, ".png") {
		println("f: $f")
		//mut name := os.file_name(f).trim_right(".png")
		mut name := os.file_name(f).all_before_last(".png")
		//println("z: $name")
		if name.count("_") != 1 {
			//idx := name.index_after(5)
			name = name.all_before_last("_")
		}
		println("name: $name")

		//tex := ctx.drawer.load_image_tex(f)
		mut tex := new_texture()
		tex.load(f)

		v.tiles[name] = tex

		v.tile_names << name
		tile_idx++

		//pp(2)
	}

	//pp(2)
}

// Draw tile at position
// XXX draw with top-left corner of 32x16 tile
// XXX add draw_tile_center function to drawing tiles at center position
[inline]
fn (mut v FieldView) draw_tile(tile_id int, px, py int) {

	//name := "tile_${tile_id}"	// XXX leak		
	//name := "tile_"	+ tile_id.str()

	//tmp_str := tile_id.str()
	//name := "tile_"	+ tmp_str
	//println("name: $name")
	//free(tmp_str)

	//tmp_str := strconv.v_sprintf("%d", tile_id)
	//tmp_str := strconv.v_sprintf("test")

	/*
	a := "World"
	s := strconv.v_sprintf("Hello %s!", a)
	println(s)
	*/

	name := v.tile_names[tile_id]
	
	mut tex := v.tiles[name]

	
	if isnil(tex) {
		println("tex is nil")
		pp(2)
	}
	
	
	mut sx, mut sy := world_to_screen_pos(px, py)

	// XXX apply half-tile yshift for 32x32 tiles
	sy -= c_cell_h
	

	//println("sx: $sx")
	//println("sy: $sx")

	tex.draw_pos(sx, sy)
	//tex.draw_pos(px, py)
}

// Draw tile at cell position
[inline]
fn (mut v FieldView) draw_tile_cell_pos(tile_id int, cx, cy int) {
	x := cx
	y := cy

	mut shx := 0
	if y % 2 != 0 {
		shx = c_cell_w / 2
	}

	mut px := x * c_cell_w + shx
	mut py := y * c_cell_h / 2

	v.draw_tile(tile_id, px, py)
}

// XXX Cell.draw
// TODO add draw_cell_at ?
//[inline]
fn (mut v FieldView) draw_cell(c &Cell) {
	cx := c.x
	cy := c.y

	v.draw_tile_cell_pos(c.tile_id, cx, cy)

	// XXX Draw cell's wall
	if !isnil(c.wall) && !isnil(c.wall.view) {
		//w := c.wall
		mut spr := c.wall.view.spr

		//pp(2)

		spr.force_draw()
	}

	// XXX Draw cell's wall
	if !isnil(c.map_obj) {
		//w := c.wall
		mut spr := c.map_obj.view.spr
		//pp(2)
		
		spr.force_draw()
	}
}

fn (mut v FieldView) spawn() {
	println("fiedview spawn()")
	
	v.load()
}

fn (mut v FieldView) draw() {
	println("fieldview draw()")

	for y in 0 .. v.m.h {
		for x in 0 .. v.m.w {
			//c := v.m.cells[x][y]
			c := &v.m.cells[x][y] // XXX ref

			//mut px := c_cell_w * x
			//mut py := c_cell_h * y

			//v.draw_tile_cell_pos(c.tile_id, x, y)
			//pp(2)

			v.draw_cell(c)
		
			//v.m.ctx.drawer.draw_tex(px, py, tex)
			//ctx.drawer.draw_tex(tex, px, py)
			//tex.draw_pos(px, py)
			//tex.draw_pos(sx, sy)
		}
	}
}