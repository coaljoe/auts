module main

import spytheman.vperlin as perlin
import time
import rand

import bits
		

fn main() {
	println("main()")

	bits.bits_main()
	//pp(2)

	// XXX init context
	//mut ctx := new_context()
	ctx = new_context()
	init_default_context() // XXX fixme?
	//init_context()

	// XXX init app
	mut app := new_app()
	
	app.init(800, 600, "auts")
	
	// XXX game
	init_game()

	//mut f := new_field()
	mut f := game.field

	f.tiles_path = "res/images/tiles/test2"

	//f.generate(4, 4)
	//f.generate(24, 24)
	//f.generate(24, 48)
	f.generate(32, 96)

	f.cells[1][1].z = 100
	println("z 0 0: ${f.cells[0][0].z}")
	println("z 1 1: ${f.cells[1][1].z}")	

	f.cells[0][1].tile_id = 1
	//f.cells[0][2].tile_id = 1
	//f.cells[0][3].tile_id = 2

	//f.cells[1][1].tile_id = 1
	//f.cells[2][2].tile_id = 2


	// Randomize tiles
	for y in 0..f.h {
		for x in 0..f.w {
			//tile_num := 3
			//tile_num := 5
			tile_num := 6
			//tile_num := f.view.tiles.keys().len - 1
			
			mut n := rand.intn(tile_num)
			
			//scale := 1.0
			//scale := 0.1
			//scale := 0.3
			scale := 0.05
			mag := f32(tile_num)
			nv := perlin.noise2d(f32(x) * scale, f32(y) * scale)
			nv_norm := (nv + 1.0) / 2.0
			xnv := nv_norm * mag
			//println(xnv)
			if xnv > tile_num {
				pp(2)
			}
			//mut n := int(xnv)
			mut noise_n := int(xnv)

			

			// Randomize with random
			//if rand.intn(100) > 80 {
			if rand.intn(100) > 70 {
			//if rand.intn(100) > 30 {
				//n = rand.intn(tile_num)
				n = noise_n
			}
			
			f.cells[x][y].tile_id = n


			// XXX place stone
			if rand.intn(100) < 1 {
				//max_variant := 4
				max_variant := 1

				mut s := new_map_obj()
				//s.load()
				//s.path = "stones/stone1"
				s.path = "trees/tree1"
				s.variant = rand.intn(max_variant)
				//s.place_at(0, 0)
				s.place_at(x, y)
				s.spawn()

				// XXX
				s.view.update(0)
			}
		}
	}

	max_variant := 4

	mut s := new_map_obj()
	//s.load()
	s.path = "stones/stone1"
	s.variant = rand.intn(max_variant)
	s.place_at(0, 0)
	s.spawn()

	// XXX
	s.view.update(0)


	//f.spawn()
	game.start()

	//d := new_drawer(mut ctx)
	mut d := ctx.drawer

	tex := d.load_image_sdl_tex("res/images/heightmap.png")
	_ = tex


	// Sprite test

	mut spr1 := new_sprite()
	spr1.load("res/objects/walls/wall1/image_0.png")

	//spr1.x = 32

	/*
	{
		// Position sprite
		px := 8
		py := 8
		// Center at tile
		spr1.x = (px * c_cell_w) + c_cell_w / 2
		spr1.y = (py * (c_cell_h / 2)) + c_cell_h / 2
	}
	*/

	mut spr2 := new_sprite()
	spr2.load("res/objects/walls/wall1/image_0.png")

	{
		// Position sprite
		px := 8
		py := 9 // XXX
		//py := 8 // XXX
		//px := 2
		//py := 2
		// Center at tile
		//spr2.x = (px * c_cell_w) + c_cell_w / 2
		//spr2.y = (py * (c_cell_h / 2)) + c_cell_h / 2
		//spr2.y = (py * (c_cell_h / 2)) + c_cell_h / 2
		//x1, y1 := cell_to_world_pos(px, py)
		x1, y1 := cell_to_world_pos_center(px, py)

		println("x1: $x1, y1: $y1")
		//pp(2)
		
		spr2.x = x1
		spr2.y = y1
	}
	
	mut wall1 := new_wall()
	wall1.spawn()

	wall1.obj.set_cx(8)
	wall1.obj.set_cy(8)

	//wall1.obj.set_cx(2)
	//wall1.obj.set_cy(1)

	//wall1.obj.set_cx(2)
	//wall1.obj.set_cy(1)

	// XXX fixme
	wall1.view.update(0)


	//ctx.vars.scroll_speed = 0
	ctx.vars.mouse_scroll = false

	game.hud.build_mode = .wall

	for app.step() {
		//app.step()
		//h.update(1)
		//game.update(1)
		//game.update(1.0 / 60.0)
		game.update(1.0 / 30.0)

		d.clear()

		//f.view.draw()
		

		//d.draw_text("test", 0, 0, d.color_white)
		
		game.draw()

		//d.draw_sdl_tex(tex, 0, 0)

		px, py := cell_to_screen_pos(1, 1)
		//px, py := cell_to_screen_pos2(1, 1)
		_ = px
		_ = py
		
		//d.draw_sdl_tex(tex, px, py)

		//spr1.draw()
		//spr2.draw()

		//wall1.view.draw()

		app.flip()

		//time.sleep_ms(20)
		time.sleep_ms(33) // 30 fps?
		//time.sleep_ms(200)
		//pp(2)
	}

	app.quit()

	println("done")
}
