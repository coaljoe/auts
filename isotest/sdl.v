module main

//type Atexit_func_t fn ()
//fn C.atexit(Atexit_func_t)
fn C.SDL_GetMouseState(x, y &int) u32
//fn C.SDL_GetKeyboardState(intptr numkeys) voidptr
//fn C.SDL_GetKeyboardState(voidptr numkeys) voidptr
fn C.SDL_GetKeyboardState(voidptr numkeys) byteptr
//fn C.SDL_GetKeyboardState(voidptr numkeys) &[]int
//fn C.SDL_GetKeyboardState(intptr numkeys) intptr
//fn C.SDL_GetKeyboardState(intptr numkeys) &int
fn C.SDL_GetScancodeFromKey(key int) int
fn C.SDL_PumpEvents()
