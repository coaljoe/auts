module main

struct MapObjView {
mut:
	m &MapObj
	spr &Sprite

	spawned bool 
}

fn new_map_obj_view(m &MapObj) &MapObjView {
	println("new_map_obj_view()")

	v := &MapObjView{
		m: m,
		spr: &Sprite(0),
	}

	return v
}

fn (mut v MapObjView) load() {
	
}

fn (mut v MapObjView) spawn() {
	println("mapobjview spawn()")

	v.load()

	v.spr = new_sprite()
	

	path := "res/objects/" + v.m.path + "/image_${v.m.variant}.png"

	v.spr.load(path)

	v.spr.visible = false
	v.spr.spawn()

	v.spawned = true
}

fn (mut v MapObjView) draw() {
	println("mapobjview draw()")

	if !v.spawned {
		return
	}

	// XXX fixme
	v.update(0)

	println("x: $v.spr.x, y: $v.spr.y")
}

fn (mut v MapObjView) update(dt f32) {
	v.spr.x = v.m.obj.x
	v.spr.y = v.m.obj.y
}