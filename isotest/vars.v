module main

struct Vars {
mut:
	res_x    int
	res_y    int

	scroll_speed  f32
	scroll_padding int

	mouse_scroll bool
}

// New default vars?
fn new_vars() &Vars {
	v := &Vars{
		scroll_speed: 450.0,
		scroll_padding: 5,
		mouse_scroll: true,
	}

	return v
}