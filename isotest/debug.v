module main

//type PPType = string | int

//pub fn pp(v int) {
fn pp(v int) {
//pub fn pp(v voidptr) {
//pub fn pp(v PPType) {
//pub fn pp(v any) {

	println(v)
	println("panic: pp")
	println("")

	//println("bt:")
	println("Backtrace:")
	print_backtrace()
	//println("bt2:")
	//print_backtrace_skipping_top_frames(3)
	//println(v)

	//println("panic $v")
	
	//panic("pp")
	//exit(v)
	exit(2)
}

/*
pub fn pp(s string) {
	println(s)
	pp(2)
}
*/