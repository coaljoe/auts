module main

import nsauzede.vsdl2
import nsauzede.vsdl2.image as img
//import time

struct App {
mut:
	w    int
	h    int
	dt   f32

	// Sdl (fixme?)
	window      voidptr
	screen      &vsdl2.Surface
	renderer    voidptr
}

fn new_app() &App {
	println("new_app()")

	a := &App{
		dt: 0.0,
		screen: &vsdl2.Surface(0),
	}

	// Update ctx
	ctx.app = a

	return a
}

fn (mut a App) init(w, h int, title string) {
	println("app init() w: $w, h: $h")

	// Init sdl
	println("init sdl...")

	C.SDL_Init(C.SDL_INIT_VIDEO)
	C.atexit(C.SDL_Quit)
	C.TTF_Init()
	C.atexit(C.TTF_Quit)
	bpp := 32
	vsdl2.create_window_and_renderer(w, h, 0, &a.window, &a.renderer)
	C.SDL_SetWindowTitle(a.window, title.str)
	a.w = w
	a.h = h
	a.screen = vsdl2.create_rgb_surface(0, w, h, bpp, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)
	//a.texture = C.SDL_CreateTexture(sdl.renderer, C.SDL_PIXELFORMAT_ARGB8888, C.SDL_TEXTUREACCESS_STREAMING, w, h)

	flags := C.IMG_INIT_PNG
	imgres := img.img_init(flags)
	if (imgres & flags) != flags {
		println('error initializing image library.')
	}

	// Init drawer
	println("init drawer...")
	
	mut d := new_drawer()
	d.init(w, h, a.renderer)

	// Update vars fixme?

	ctx.vars.res_x = w
	ctx.vars.res_y = h

	println("app init done")
}

fn (mut a App) get_mouse_state() (int, int) {
	x := 0
	y := 0
	C.SDL_GetMouseState(&x, &y)

	return x, y
}

/*
fn (mut a App) get_keyboard_state() voidptr {
	state := C.SDL_GetKeyboardState()

	return state
}
*/

fn (mut a App) is_key_pressed(key int) bool {
	//state := C.SDL_GetKeyboardState(voidptr(0))
	//state := C.SDL_GetKeyboardState(intptr(0))
	//numkeys := &int(0)
	//state := C.SDL_GetKeyboardState(numkeys)
	numkeys := int(0)
	state := C.SDL_GetKeyboardState(&numkeys)
	skey := C.SDL_GetScancodeFromKey(key)

	//println("state: $state")
	//println("skey: $skey")

	mut pressed := false

	mut d := &byte(0)
	
	unsafe {
		d = &state[0]
		println("d1:")
		//println(d.len)
		//println(d[0])
		//println(d[1])
		//println(d[255])
		//println(d[key])

		//if d[255] > 0 {
		//if d[79] > 0 {
		//if d[82] > 0 { // Up
		if d[skey] > 0 { 
			println("test")
			pressed = true
		}
	}
	
	/*
	
	//mut m_state := &int(0)
	//mut m_state := &[]int{}
	//mut m_state := []&int{}
	//mut z := []int{}
	mut z := []byte{}
	mut z_ptr := &z

	//m_state = state
	z_ptr = state

	println("z_ptr:")
	println(z_ptr)
	println(z_ptr.len)

	q := *z_ptr

	println("q:")
	println(q)
	println(q.len)

	*/

	/*
	unsafe {
		//m_state = state
	}
	*/
	

	//println("len v: ${m_state.len}")
	//println(m_state)
	//println(m_state.len)

	//v := m_state[0]

	//println("v: $v")	
	
	//println("skey: $skey")
	//pp(2)

	//return false
	return pressed
}

fn (mut a App) pump_events() {
	C.SDL_PumpEvents()
}

fn (mut a App) step() bool {
	println("app step()")

	a.dt = f32(20.0)
	
    mut should_close := false
    //for {
        //evt := SDL_Event{}
        evt := vsdl2.Event{}
       	for 0 < vsdl2.poll_event(&evt) {
            match int(evt.@type) {
                C.SDL_QUIT { should_close = true }

                C.SDL_KEYDOWN {
					key := evt.key.keysym.sym
					if key == C.SDLK_ESCAPE || key == C.SDLK_q {
					    should_close = true
					    break
					}
				}

				else {}
            }

        }
    //}

    if should_close {
    	//pp(2)
        return false
    }

	//a.render()

	a.update(a.dt)

	return true
}

fn (mut a App) flip() {
	println("app flip()")

	C.SDL_RenderPresent(a.renderer)
}

fn (mut a App) quit() {
	println("app quit()")

	//C.SDL_DestroyRenderer(a.renderer)
    C.SDL_DestroyWindow(a.window)
    C.SDL_Quit()
}

fn (mut a App) update(dt f32) {

}
