module main

struct Texture {
mut:
	w int
	h int

	// Sdl
	tex voidptr
}

fn new_texture() &Texture {
	println("new_texture()")

	t := &Texture{
		tex: voidptr(0),
	}

	return t
}

fn (mut t Texture) size() (int, int) {
	return t.w, t.h
}

fn (mut t Texture) load(path string) {
	println("texture load() path: $path")

	t.tex = ctx.drawer.load_image_sdl_tex(path)

	if isnil(t.tex) {
		println("tex is nil")
		pp(2)
	}

	t.w, t.h = ctx.drawer.get_sdl_tex_size(t.tex)

	if t.w < 1 || t.h < 1 {
		println("bad tex size")
		pp(2)
	}
}

// Draw at position
fn (mut t Texture) draw_pos(x, y int) {
	//println("texture draw_pos() x: $x, y: $y")

	//ctx.drawer.draw_sdl_tex(t.tex, x, y)
	ctx.drawer.draw_tex(t, x, y)
}