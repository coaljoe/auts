module main

// Game/GameState
struct Game {
mut:
	field     &Field
	hud       &Hud
	spritesys &SpriteSys
	viewport  &Viewport
}

fn init_game() {
	println("init_game()")

	g := &Game{
		field: new_field(),
		hud: new_hud(),
		spritesys: new_spritesys(),
		viewport: new_viewport(),
	}

	game = g
}

fn (mut g Game) start() {
	g.field.spawn()
}

fn (mut g Game) draw() {
	g.field.draw()
	g.spritesys.draw()
	g.hud.draw()
}

fn (mut g Game) update(dt f32) {
	g.field.update(dt)
	g.spritesys.update(dt)
	g.hud.update(dt)
	g.viewport.update(dt)
}