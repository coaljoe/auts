#!/bin/bash

#export LIBGL_ALWAYS_SOFTWARE=1

# rotate log
cp log log_old
echo -n > log


#v run main.v
#v run .
#v --enable-globals -g -cg run . |& tee log
#stdbuf -o 0 v --enable-globals -g -cg run . 2>&1 | tee log
unbuffer v --enable-globals -g -cg run . 2>&1 | tee log
