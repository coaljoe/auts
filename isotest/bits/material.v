module bits

import bits.fixed

/*
// Material Elem Record
struct MaterialElem {
mut:
	// n_bits
	bits_size int
	elem &Elem
}
*/


// Many objects can share the same material
// Can be compound / multiple elements per material
// XXX TODO: ? bits amount values / calculations in percentages?
// XXX like 20% wood 80% grass?
// XXX how to calculate the amount of material/bits?
// XXX via object mass/weight (must be specified)?

// XXX or 10% glass 90% concrete?
// XXX do we really need such materials?

//struct ObjectPart {
struct MaterialPart {
pub mut:
	name string
	elem &Elem
	//bits_size int
	bits_size fixed.Fixed
}

// MaterialInfo information about materials in object

// PartsInfo
//struct PartsInfo {
struct MaterialInfo {
pub mut:
	//elems map[string]&Elem
	//elems map[string]&MaterialElem
	elems map[string]&MaterialPart
}

pub fn new_material_info() &MaterialInfo {
	mi := &MaterialInfo{}

	return mi
}

pub fn (mi &MaterialInfo) has_elem(elem &Elem) bool {
	return !isnil(mi.elems[elem.name])
}

pub fn (mi &MaterialInfo) has_elem_name(elem_name string) bool {
	return !isnil(mi.elems[elem_name])
}

// Add elem record
pub fn (mut mi MaterialInfo) add_elem(bits_size fixed.Fixed, elem &Elem) {
	println("MaterialInfo add_elem: bits_size: $bits_size, elem.name: $elem.name")

	pt := &MaterialPart{
		elem: elem,
		bits_size: bits_size,
	}
	mi.elems[elem.name] = pt
}

// Get part by elem name
pub fn (mi &MaterialInfo) get_elem_name(elem_name string) &MaterialPart {
	
	pt := mi.elems[elem_name]
	
	//if isnil(me) {
	//	panic(2)
	//}

	return pt
}

// From volume m3
pub fn (mut mi MaterialInfo) add_elem_from_volume(vol f32, elem &Elem) {
	println("MaterialInfo add_elem_from_volume: vol: $vol, elem.name: $elem.name")

	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(vol_bits, elem)
}

// From volume mass/weight
pub fn (mut mi MaterialInfo) add_elem_from_mass(mass f32, elem &Elem) {
	println("MaterialInfo add_elem_from_mass: mass: $mass, elem.name: $elem.name")

	vol := mass / f32(elem.density)

	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(vol_bits, elem)
}

//fn (mut mi MaterialInfo) add_elem_from_size(size [3]f32, elem &Elem) {
//fn (mut pi PartsInfo) add_elem_from_size(size []f32, elem &Elem) {
//	println("PartsInfo add_elem_from_size: $size, elem.name: $elem.name")
pub fn (mut mi MaterialInfo) add_elem_from_size(mx, my, mz f32, elem &Elem) {
	println("MaterialInfo add_elem_from_size: mx: $mx, my: $my, mz: $mz, elem.name: $elem.name")

	/*
	mx := size[0]
	my := size[1]
	mz := size[2]
	*/
	
	vol := mx * my * mz
	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(vol_bits, elem)
}


pub fn (mi &MaterialInfo) elems_list() []&MaterialPart {
	mut ret := []&MaterialPart{}
	for k in mi.elems.keys() {
		v := mi.elems[k]
		ret << v
	}
	
	return ret
}
