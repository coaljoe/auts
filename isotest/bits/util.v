module bits

//import math
import bits.fixed

[inline]
fn bits_to_m(n_bits fixed.Fixed) f32 {
	//return f32(n_bits) * c_bit_size / 100.0
	return f32(n_bits.value()) / c_bit_size
}

[inline]
fn m_to_bits(m f32) fixed.Fixed {
	return fixed.new_f(m * 100.0 / c_bit_size)
}

[inline]
fn bits_to_m3(n_bits fixed.Fixed) f32 {
	//return f32(n_bits) / (math.powf(c_bit_size, 3))
	return f32(n_bits.value()) / c_bits_in_m3
}

[inline]
fn m3_to_bits(m3 f32) fixed.Fixed {
	//return int(m3 * 1000 / (math.powf(c_bit_size, 3)))
	return fixed.new_f(m3 * c_bits_in_m3)
}

[inline]
fn m_to_cm(m f32) f32 {
	return m * 100.0
}

[inline]
fn cm_to_m(cm f32) f32 {
	return cm / 100.0
}