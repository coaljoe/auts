module bits

fn test_wall() {
	println("bits: test_wall()")

	// Wall is 1x1x6 m in size

	// Create wall
	
	mut w := new_object()
	w.name = "wall"

	w.mat_info.add_elem_from_size(1, 1, 6, bits_ctx.elem_lib["stone"])

	// Crush entire wall

	mut gcp := new_generic_crusher_processor()
	gcp.process(mut w)

	println("w new bits volume: ${w.bits_volume()}")
	println("w new volume: ${w.volume()}")

	println("bits: done test_wall")
}

fn test_bow() {
	println("bits: test_bow()")

	mut bow := new_object()

	// m3
	bow.mat_info.add_elem_from_volume(0.88, bits_ctx.elem_lib["wood"])
	

	println("bow new bits volume: ${bow.bits_volume()}")
	println("bow new volume: ${bow.volume()}")

	// XXX
	// no bowstring: hemp/fiber

	bowstring_l := f32(0.7) // 70cm

	// Actual lenght
	rope_l := bowstring_l * 4.0 // twisted rope
	rope_d := f32(0.04) // 4mm
	rope_v := rope_l * rope_d // approx.

	println("rope_v: $rope_v")
	
	bow.mat_info.add_elem_from_volume(rope_v, bits_ctx.elem_lib["textile"])

	println("bow new bits volume: ${bow.bits_volume()}")
	println("bow new volume: ${bow.volume()}")

	// XXX test arrows

	mut arrow := new_object()

	// Arrowhead weight: 200g
	// Arrow shaft size: 1x1x50 cm

	arrow.mat_info.add_elem_from_mass(0.2, bits_ctx.elem_lib["iron"])
	arrow.mat_info.add_elem_from_size(
		cm_to_m(1), cm_to_m(1), cm_to_m(50), bits_ctx.elem_lib["wood"])

	println("arrow new bits volume: ${arrow.bits_volume()}")
	println("arrow new volume: ${arrow.volume()}")

	println("bits: done test_bow")
}

pub fn bits_main() {
	println("bits: bits_main()")

	// Init
	bits_ctx = bits.new_context()

	init_elem_lib()
	
	//
	// Effects
	//

	println("effects")
	
	mut ef := new_effect()
	ef.name = "melt"
	ef.in_elem = bits_ctx.elem_lib["sand"]
	ef.out_elem = bits_ctx.elem_lib["glass"]
	ef.cond_temp = 5000

	// XXX bug: can't print recursive elems?
	//println("ef: ${ef}")

	//
	// Objects
	//

	println("objects")

	mut ob := new_object()
	ob.name = "stone1"
	//ob.dim_x = 2
	//ob.dim_y = 2
	//ob.dim_z = 2
	//ob.set_size(2, 2, 2)
	//ob.set_size(1, 1, 1)
	println("size: ${ob.size()}")
	println("volume: ${ob.volume()}")
	println("bits volume: ${ob.bits_volume()}")
	//ob.elem = e4
	//ob.mat_info.add_elem_from_size([2, 2, 2], e4)
	//ob.parts_info.add_elem_from_size([f32(2.), 2., 2.], e4)
	//ob.parts_info.add_elem_from_size(2, 2, 2, e4)
	ob.mat_info.add_elem_from_size(2, 2, 2, bits_ctx.elem_lib["stone"])

	// Position
	//ob.transform.pos_x = 10.0
	//ob.transform.pos_y = 10.0
	//ob.transform.rot_z = 45.0
	
	ob.pos_x = 10
	ob.pos_y = 10
	//ob.transform.rot_z = 45.0 // XXX?

	// XXX
	//println("ob: ${ob}")

	println("ob volume: ${ob.volume()}")
	println("ob mass: ${ob.mass()}")

	//
	// Processors
	//

	println("processors")

	mut prc := new_processor()
	prc.name = "crushed_stone"
	prc.in_elem = bits_ctx.elem_lib["stone"]
	//prc.out_elem = e4
	//prc.output_factor = 0.6
	//prc.req_electricity = true
	//prc.req_power = 1000

	// XXX
	//println("prc: ${prc}")

	//
	// Universal Processors?
	// 

	/*
	prc2 := new_generic_processor()
	prc2.name = "generic_crusher"
	prc2.req_solid = true
	//prc2.in_elem =
	prc2.process(ob)
	*/

	mut gcp := new_generic_crusher_processor()
	gcp.process(mut ob)

	println("new bits volume: ${ob.bits_volume()}")
	println("new volume: ${ob.volume()}")
	//println("new elem name: ${ob.elem.name}")

	//e4.generic_processor0 = gcp

	// Try to process itself

	//ob.elem.generic_processor0.process(ob)
	//e4.generic_processor0.process(ob)

	//
	// World
	//

	mut w := new_world()

	b1 := Bit{
		elem_type: .wood,
	}
	
	w.bits[0][0] = b1

	println("w b1: ${w.bits[0][0]}")
	println("w 1 0: ${w.bits[1][0]}")
	//println("w: ${w}")
	//println("w: ${w.bits}")

	//
	// Complex object
	//

	mut ob2 := new_object()
	ob2.name = "pipe" // Reinforced-concrete pipe, a 3d object
	//ob2.elem = e_concrete


	println("")
	
	//test_wall()
	test_bow()

	println("done")
}
