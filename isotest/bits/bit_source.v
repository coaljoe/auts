module bits

// World bit source (emits bits)
struct BitSource {
mut:
	pos WorldPos
	elem_type ElemType
	rate f32
	active bool
}

fn new_bit_source() &BitSource {
	bs := &BitSource{
		active: true,
	}
	
	return bs
}

fn (bs &BitSource) update(dt f32) {

}
