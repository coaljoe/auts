module bits

// XXX specific processor
// XXX objects are data-driven ? fox flexibility?
struct StoneProcessor {

}

fn (p &StoneProcessor) crush_object_part(mut ob Object) {
	// XXX
	stone_crush_object_part(mut ob)
}

// Create new stone bit object
fn new_stone_bit() &Object {
	//println("new_stone_bit")
	
	mut ob := new_object()
	//ob.elem = bits_ctx.elem_lib["stone"]

	return ob
}

fn crush_stone_bit() {

}

/*
// stone -> crushed stone
// XXX turn stone object into crushed stone object
// XXX all volume
// XXX this can be created/used as a factory (?)
fn crush_stone_object2(ob &Object) {
	println("crush_stone_object2")
	// TODO: amt, force, fracture_force?

	// XXX crush entire object

	num_bits := ob.bits_volume()

	mut prc := new_processor()
	prc.name = "crushed_stone"
	prc.in_elem = bits_ctx.elem_lib["stone"]
	prc.out_elem = bits_ctx.elem_lib["crushed_stone"]
	prc.output_factor = 0.6
	prc.req_electricity = true
	prc.req_power = 1000

	// Checks
	if isnil(prc.in_elem) {
		panic("bad in_elem")
	}
	if isnil(prc.out_elem) {
		panic("bad out_elem")
	}

	prc.process(ob)
}
*/

// XXX should be registered on facrory
// XXX rename to stole_crush_object() ?
fn stone_crush_object_part(mut ob Object) {
	println("stone_crush_object_part")
	// TODO: amt, force, fracture_force?

	// XXX crush entire object part

	//for pt in ob.parts_info.elems_list() {

		num_bits := ob.bits_volume()

		mut prc := new_processor()
		prc.name = "crushed_stone"
		prc.in_elem = bits_ctx.elem_lib["stone"]

		mut rec := new_processor_out_elem_rec()
		rec.out_elem = bits_ctx.elem_lib["crushed_stone"]
		rec.output_factor = 0.6
		rec.replaces_in_elem = true
		rec.req_electricity = true
		rec.req_power = 1000

		prc.out_elems << rec

		mut rec2 := new_processor_out_elem_rec()
		rec2.out_elem = bits_ctx.elem_lib["sand"]
		rec2.output_factor = 0.1
		rec2.req_electricity = true
		rec2.req_power = 1000

		prc.out_elems << rec2

		// Checks
		if isnil(prc.in_elem) {
			panic("bad in_elem")
		}
		if prc.out_elems.len < 1 {
			panic("bad out_elem")
		}

		ret := prc.process(mut ob)
		//prc.process_part(pt)

		if ret.len > 0 {
			println("ret len: ${ret.len}")
			println("ret name: ${ret[0].elem.name}")
			println("ret bits_size: ${ret[0].bits_size}")
		}

	//}
}

// stone -> crushed stone
fn crush_stone_object(mut ob Object) {
	println("crush_stone_object")
	// TODO: amt, force, fracture_force?

	// XXX crush entire object

	num_bits := int(ob.bits_volume().value()) // XXX

	// Create separate objects
	for i in 0..num_bits {

		// Detach bits / bit objects from main object
		b := new_stone_bit()

		// XXX place it somewhere in the world
		// just nearby crushed object

		// physics needed?
		// not for objects processed in processors?
	}

	// XXX todo destroy main object
}
