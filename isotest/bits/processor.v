module bits

import bits.fixed

// Out elem record/info
// One to [Many]
struct ProcessorOutElemRec {
mut:
	//out_elems map[string]&Elem
	out_elem &Elem

	output_factor f32

	replaces_in_elem bool

	cond_temp int
	req_electricity bool
	req_power int
	req_steam int
	req_water int
}

fn new_processor_out_elem_rec() &ProcessorOutElemRec {
	r := &ProcessorOutElemRec{
		out_elem: &Elem(0),
	}
	return r
}

// A convertor from one element to another
struct Processor {
mut:
	name string
	// Always one [One]
	in_elem &Elem
	//out_elem &Elem
	// Always many [Many]
	out_elems []&ProcessorOutElemRec
	//out_elems_map map[string][]&ProcessorOutputTableRecord

	//output_factor f32

	//cond_temp int
	//req_electricity bool
	//req_power int
	//req_steam int
	//req_water int
}

fn new_processor() &Processor {
	p := &Processor{
		in_elem: &Elem(0),
		//out_elem: &Elem(0),
	}
	return p
}

fn (p &Processor) process(mut o Object) []&MaterialPart {
	println("processor process")

	mut ret := []&MaterialPart{}

	mut processed := false

	// Process all elems
	mut pts_list := o.mat_info.elems_list()
	for i, _ in pts_list {
		mut pt := pts_list[i]

		println("pt.elem: $pt.elem.name")
		println("pt.bits_size: $pt.bits_size")

		// XXX match, process part/elem
		if pt.elem.name == p.in_elem.name { // XXX fixme
			println("process for ${pt.elem.name}")

			// Save initial size
			orig_pt_bits_size := pt.bits_size.value()

			for out_elem_rec in p.out_elems {

				println("in elem: $p.in_elem.name")
				println("out elem: $out_elem_rec.out_elem.name")

				new_elem := out_elem_rec.out_elem

				if isnil(new_elem) {
					println("error: new_elem cannot be nil")
					exit(2)
				}

				//new_bits_size := int(pt.bits_size * out_elem_rec.output_factor)
				new_bits_size := fixed.new_f(orig_pt_bits_size * out_elem_rec.output_factor)

				println("new_elem: ${isnil(new_elem)}")
				println("new_elem: $new_elem")
				println("new_bits_size: $new_bits_size")

				// Swap elems
				if out_elem_rec.replaces_in_elem {
					println("replace elem...")
					pt.elem = new_elem
					pt.bits_size = new_bits_size // XXX fixme?
				} else {
					println("create new elem...")
					// Create new elems
					mat_pt := &MaterialPart{
						elem: new_elem,
						bits_size: new_bits_size,
					}
					ret << mat_pt
				}

				processed = true
			}
		}
	}

	if !processed {
		println("failed to process")
	}

	println("done processing")

	return ret
}
