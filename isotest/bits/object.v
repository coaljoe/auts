module bits

import bits.fixed

// Object bit
//  size: 10x10x10 (cm) (?)
//
// XXX cannot be compound?
struct ObjectBit {
	elem_type ElemType
}

struct Object {
pub mut:
	name string
	
	// Dimentions in bits
	// XXX optional, not used.
	//dim_x int // Approx.
	//dim_y int
	//dim_z int
	// XXX use volume to calculate object bounds.

	// Material/Elem
	//elem &Elem // XXX use id/enum
	mat_info &MaterialInfo
	//parts_info &PartsInfo

	// Position in the world
	// XXX in map cells/coords?
	// XXX approximate, limited precision
	pos_x int
	pos_y int
	pos_z int

	// Flixible position in the world
	//transform &Transform

	// Object size/volume in bits
	// amount of bits
	//bits_size int

	// Bits of object's voxel model
	//bits [10][10]ObjectBit
}

pub fn new_object() &Object {
	println("bits: new_object()")
	
	o := &Object{
		//elem: &Elem(0),
		//mat_info: &MaterialInfo(0),
		//parts_info: &PartsInfo(0),
		//parts_info: new_parts_info(),
		mat_info: new_material_info(),
		//transform: new_transform(),
	}
	
	return o
}

// Calc total volume in bits
pub fn (o &Object) bits_volume() fixed.Fixed {
	//return o.dim_x * o.dim_y * o.dim_z
	//return o.bits_size

	mut ret := fixed.new_f(0.0)
	for me in o.mat_info.elems_list() {
		ret = ret.add(me.bits_size)
	}
	
	return ret
}

// Calc total volume in m3
pub fn (o &Object) volume() f32 {
	return bits_to_m3(o.bits_volume())
}

// Calc total mass in kg
pub fn (o &Object) mass() f32 {

	mut total_mass := f32(0.0)

	for _, me in o.mat_info.elems {

		mat_density := me.elem.density
		mat_volume := f32(me.bits_size.value())

		// XXX is this correct?
		// shouldn't this be in m3 instead?
		mass := mat_density * mat_volume

		total_mass += mass
	}

	return total_mass
}

/*
// Set size in meters
fn (mut o Object) set_size(mx, my, mz f32) {
	println("object set_size: mx: $mx, my: $my, mz: $mz")
	
	/*
	scale := f32(100.0 / c_bit_size)
	o.dim_x = int(mx * scale)
	o.dim_y = int(my * scale)
	o.dim_z = int(mz * scale)
	*/

	/*
	o.dim_x = m_to_bits(mx)
	o.dim_y = m_to_bits(my)
	o.dim_z = m_to_bits(mz)
	*/

	vol := mx * my * mz
	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")
	//panic(2)
	//exit(2)

	o.bits_size = vol_bits
}
*/

// Get size in m3
pub fn (o &Object) size() f32 {
	return o.volume()
}

/*
// Get size in meters
fn (o &Object) size() (f32, f32, f32) {
	/*
	scale := f32(c_bit_size / 100.0)
	mx := o.dim_x * scale
	my := o.dim_y * scale
	mz := o.dim_z * scale
	*/
	mx := bits_to_m(o.dim_x)
	my := bits_to_m(o.dim_y)
	mz := bits_to_m(o.dim_z)
	
	return mx, my, mz
}
*/
