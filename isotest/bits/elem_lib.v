module bits

fn init_elem_lib() {

	{
		mut el := new_elem()
		el.name = "stone"
		el.elem_type = .stone
		el.density = 2700 // wiki:limestone

		bits_ctx.elem_lib["stone"] = el
	}

	{
		mut el := new_elem()
		el.name = "crushed_stone"
		el.elem_type = .stone // XXX?
		el.density = 2700 // XXX?
						  // custom material/element density?

		el.aggregate_state = .crushed

		bits_ctx.elem_lib["crushed_stone"] = el
	}

	{
		mut el := new_elem()
		el.name = "sand"
		el.elem_type = .sand
		el.density = 1600 // wiki

		bits_ctx.elem_lib["sand"] = el
	}

	{
		mut el := new_elem()
		el.name = "glass"
		el.elem_type = .glass
		el.density = 2500 // wiki

		bits_ctx.elem_lib["glass"] = el
	}

	{
		mut el := new_elem()
		el.name = "wood"
		el.elem_type = .wood
		el.density = 700 // wiki
		el.flameable = true

		bits_ctx.elem_lib["wood"] = el
	}

	{
		mut el := new_elem()
		el.name = "textile"
		el.elem_type = .textile
		el.density = 1500 // hemp/flax m3 density
		el.flameable = true

		bits_ctx.elem_lib["textile"] = el
	}

	{
		mut el := new_elem()
		el.name = "iron" // XXX wrought iron
		el.elem_type = .iron
		el.density = 7870 // wiki (iron)
		el.flameable = false
		el.notes = "wrought iron"

		bits_ctx.elem_lib["iron"] = el
	}

	{
		mut el := new_elem()
		el.name = "water"
		el.elem_type = .water
		//el.density = 10000
		el.density = 1000 // wiki
		el.liquid = true

		bits_ctx.elem_lib["water"] = el
	}	

	{
		mut el := new_elem()
		el.name = "concrete"
		el.elem_type = .concrete
		//el.density = 20000
		el.density = 2400 // wiki

		bits_ctx.elem_lib["concrete"] = el
	}	
}
