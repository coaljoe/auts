module main

struct Field {
mut:
	w int
	h int
	tiles_path string
	cells [][]Cell
	generated bool
	
	view &FieldView
}

fn new_field() &Field {
	println("new_field()")
	f := &Field{
		tiles_path: "res/images/tiles/test1",
		view: &FieldView(0),
	}
	return f
}

fn (mut f Field) generate(w, h int) {
	println("f.generate w: $w, h: $h")
	
	f.w = w
	f.h = h

	println("generate cells...")
	f.cells = [][]Cell{ len: w } // Rows
	//for y in 0..h {
	//	f.cells[y] = []Cell{ len: h } // Cols
	//}

	for i in 0..f.cells.len {
		f.cells[i] = []Cell{ len: h } // Cols
	}

	// Create cells
	for y in 0..f.h {
		for x in 0..f.w {
			mut c := new_cell()
			c.x = x
			c.y = y
			f.cells[x][y] = c
		}
	}

	f.generated = true
}

fn (f &Field) check_coords(cx, cy int) bool {
	if cx < 0 || cx > f.w-1 || cy < 0 || cy > f.h-1 {
		return false
	}
	return true
}

// Get cell as reference
fn (f &Field) get_cell(cx, cy int) &Cell {
	if !f.check_coords(cx, cy) {
		println("panic: bad coords: cx: $cx, cy: $cy")
	}

	c := &f.cells[cx][cy]
	return c
}

fn (f &Field) width_px() int {
	return f.w * c_cell_w
}

fn (f &Field) height_px() int {
	return f.h * c_cell_h
}

fn (mut f Field) spawn() {
	f.view = new_fieldview(f)
	f.view.spawn()
}

fn (mut f Field) draw() {
	f.view.draw()
}

fn (mut f Field) update(dt f32) {

}